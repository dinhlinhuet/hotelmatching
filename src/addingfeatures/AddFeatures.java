package addingfeatures;
import handlingfile.ManipulateFile;
import hotel.Hotel;
import info.debatty.java.stringsimilarity.Jaccard;
import info.debatty.java.stringsimilarity.Levenshtein;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;

import measures.CosineSimilarity;
import measures.HeadOfStringRules;
import measures.KmDistance;
import preprocessdata.RemoveStopword;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/*This class is for adding full of features to base dataset by using similarity metrics*/
public class AddFeatures extends ManipulateFile{
    static FastVector atts = new FastVector();
    static Instances data1;
    static double[] vals;
    static ArrayList<Attribute> attributes = new ArrayList<>();
    static ArrayList<Double> valsOfFeats = new ArrayList();
    static double[] valsOfFeat = new double[45];
    static RemoveStopword[] removeStopword = new RemoveStopword[3];
    static Hotel[] hotelPair = new Hotel[2];
    static CosineSimilarity cosineSimilarity = new CosineSimilarity();

    public Instances addingFeatures(String input) {
        removeStopword[0] = new RemoveStopword();
        removeStopword[1] = new RemoveStopword();
        removeStopword[2] = new RemoveStopword();

        removeStopword[0].readStopWords("D:/mega folder/thesis/stopword3.txt");
        removeStopword[1].readStopWords("D:/mega folder/thesis/stopword1.txt");
        removeStopword[2].readStopWords("D:/mega folder/thesis/stopword.txt");
        KmDistance kmDistance = new KmDistance();
        Levenshtein l = new Levenshtein();
        Jaccard jc = new Jaccard();
        HeadOfStringRules headOfStringRules = new HeadOfStringRules();
        ArffLoader loader = new ArffLoader();
        try {
            loader.setFile(new File(input));
            Instances data = loader.getDataSet();
            int i = 0, k = 0;
            FastVector myNomVals = new FastVector();
            myNomVals.addElement("0");
            myNomVals.addElement("1");
            FastVector nomVals = new FastVector();
            nomVals.addElement("-1");
            nomVals.addElement("0");
            nomVals.addElement("1");
            attributes.add(new Attribute("Levenshtein_name"));
            attributes.add(new Attribute("Levenshtein_address"));
            attributes.add(new Attribute("Jaccard_name"));
            attributes.add(new Attribute("Jaccard_address"));
            attributes.add(new Attribute("Cosine_similarity_name"));
            attributes.add(new Attribute("Cosine_similarity_address"));
            attributes.add(new Attribute("Removed_stopwords_name1", (FastVector) (null)));
            attributes.add(new Attribute("Removed_stopwords_name2", (FastVector) (null)));
            attributes.add(new Attribute("Removed_stopwords_address1", (FastVector) (null)));
            attributes.add(new Attribute("Removed_stopwords_address2", (FastVector) (null)));
            attributes.add(new Attribute("star"));
            attributes.add(new Attribute("geolocation"));
            attributes.add(new Attribute("variant_lat"));
            attributes.add(new Attribute("variant_long"));
            for (k = 0; k < 45; k++) {
                attributes.add(new Attribute("Similar_stopword" + k));
            }
            attributes.add(new Attribute("class", myNomVals));
            for (k = 0; k < 60; k++) {
                atts.addElement(attributes.get(k));
            }
            data1 = new Instances("Relation", atts, 0);
            i = 0;
            while (i < data.numInstances()) {
                String hotel1 = null, hotel2 = null;
                String hotel3 = null, hotel4 = null;
                String hotel5 = null, hotel6 = null;
                vals = new double[data1.numAttributes()];
                int j = 0;
                for (j = 0; j < 2; j++) {
                    if (j == 0) {
                        hotelPair = removeStopword[j].removeStopWords(data.instance(i).stringValue(2 * j), data.instance(i).stringValue(2 * j + 1), 0);
                        hotelPair = removeStopword[j + 1].removeStopWords(hotelPair[0].trimmedHotelName, hotelPair[1].trimmedHotelName, j + 1);
                    }
                    if (j == 1)
                        hotelPair = removeStopword[j + 1].removeStopWords(data.instance(i).stringValue(2 * j), data.instance(i).stringValue(2 * j + 1), j + 1);
                    if (j == 0) {
                        hotel1 = hotelPair[0].trimmedHotelName;
                        hotel2 = hotelPair[1].trimmedHotelName;
                    } else {
                        hotel1 = hotelPair[0].trimmedHotelAddress;
                        hotel2 = hotelPair[1].trimmedHotelAddress;
                    }
                    hotel3 = Normalizer.normalize(hotel1, Normalizer.Form.NFD).replaceAll("\\p{M}", "").replaceAll("đ", "d");
                    hotel5 = hotel3;// keep comma to use for headofStringRules
                    hotel3 = hotel3.replaceAll("(,)|\\p{Punct}*$|(,+\\s*,+)|(-)", "");
                    hotel4 = Normalizer.normalize(hotel2, Normalizer.Form.NFD).replaceAll("\\p{M}", "").replaceAll("đ", "d");
                    hotel6 = hotel4;
                    hotel4 = hotel4.replaceAll("(,)|\\p{Punct}*$|(,+\\s*,+)(-)", "");
                    vals[2 * j + 6] = data1.attribute(2 * j + 6).addStringValue(hotel3);
                    vals[2 * j + 7] = data1.attribute(2 * j + 7).addStringValue(hotel4);
                    vals[j] = l.distance(hotel3, hotel4);
                    vals[j + 2] = jc.similarity(hotel3, hotel4);
                    vals[j + 4] = cosineSimilarity.getCosineSimilarity(hotel3, hotel4);
                }
                vals[j + 8] = Math.abs(data.instance(i).value(6) - data.instance(i).value(7));
                vals[j + 9] = kmDistance.haversine(data.instance(i).value(8), data.instance(i).value(10), data.instance(i).value(9)
                        , data.instance(i).value(11));
                vals[j + 10] = Math.abs(data.instance(i).value(8) - data.instance(i).value(9));
                vals[j + 11] = Math.abs(data.instance(i).value(10) - data.instance(i).value(11));
                valsOfFeat = headOfStringRules.createRulesHeadofString(hotel5, hotel6);
                for (k = 0; k < 45; k++) {
                    vals[j + 12 + k] = valsOfFeat[k];
                }
                vals[j + 12 + k] = data.instance(i).value(12);
                Instance instance = new Instance(1.0, vals);
                data1.add(instance);
                i++;
            }
            System.out.println(data1.numInstances());
            System.out.println(data1.numAttributes());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return data1;
    }

    public static void main(String[] args) {
        AddFeatures addFeatures = new AddFeatures();
        addFeatures.addingFeatures("D:/mega folder/thesis/data/test_data_sub1.1.arff");
        String tailPath = "1.x";
        String outputPath = "D:/mega folder/thesis/data/data";
        saveFile(data1,outputPath + tailPath + ".arff");
    }
}
