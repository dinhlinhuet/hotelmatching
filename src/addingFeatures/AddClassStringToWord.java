package addingfeatures;

import java.io.File;
import java.io.IOException;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;

public class AddClassStringToWord {
    static Instances data, data1;
    public static void main(String[] args) {
        ArffLoader arffLoader = new ArffLoader();
        try {
            String pathFile = "D:\\OneDrive\\Documents\\thesis\\data\\test_data_sub1_str_t_w_vtr.arff";
            String pathFile1 = "D:\\OneDrive\\Documents\\thesis\\data\\test_data_sub1.arff";
            arffLoader.setFile(new File(pathFile));
            data = arffLoader.getDataSet();
            arffLoader.setFile(new File(pathFile1));
            data1 = arffLoader.getDataSet();
            FastVector fastVector = new FastVector();
            fastVector.addElement("0");
            fastVector.addElement("1");
            data.insertAttributeAt(new Attribute("unique", fastVector), data.numAttributes());
            int i = 0;
            while (i < data.numInstances()) {
                data.instance(i).setValue(data.numAttributes() - 1, data1.instance(i).value(data1.numAttributes() - 1));
                System.out.println(data.instance(i).stringValue(data.numAttributes() - 1));
                i++;
            }
        } catch (IOException exception) {
            // TODO Auto-generated catch block
            exception.printStackTrace();
        }
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(data);
            String tailPath = "_sub1_str_t_w_vtr1";
            String outputPath = "D:/OneDrive/Documents/thesis/data/test_data";
            File file = new File(outputPath + tailPath + ".arff");
            if (!file.exists()) {
                System.out.println("create");
                saver.setFile(file);
                saver.writeBatch();
            } else {
                System.out.println("create1");
                saver.setFile(file);
                saver.writeBatch();
            }
        } catch (IOException exception) {
            System.out.println("file has already existed");
        }

    }

}
