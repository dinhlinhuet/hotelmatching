package handlingfile;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;

/**
 * Created by warrior on 7/2/2016.
 */
public class ManipulateFile {
    public static void saveFile(Instances data, String url){
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(data);
            File file = new File(url);
            if (!file.exists()) {
                System.out.println("create");
                saver.setFile(file);
                saver.writeBatch();
            } else {
                System.out.println("create1");
                saver.setFile(file);
                saver.writeBatch();
            }
        } catch (IOException e) {
            System.out.println("file has already existed");
        }
    }
}
