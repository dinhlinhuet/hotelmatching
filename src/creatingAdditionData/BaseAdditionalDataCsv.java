package creatingAdditionData;
/*Because there're two file dataset at first then we have to read both and merge them to one dataset.
* This is only base data with original features*/
import java.io.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import handlingfile.ManipulateFile;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

public class BaseAdditionalDataCsv extends ManipulateFile{
	static FastVector atts = new FastVector();;
	static Instances data, data1;
	static double[] vals;
	int i;
	List<String> line_temp = new ArrayList<String>();
	ArrayList<FastVector> list_vector = new ArrayList<>();
	ArrayList<String> nameAttributes = new ArrayList<>();
	static ArrayList<Attribute> attributes = new ArrayList<>();
	static HashSet hashset_url1 = new HashSet<>();
	static HashSet hashset_url2 = new HashSet<>();
	static HashSet hashset_url3 = new HashSet<>();
	static HashSet hashset_url4 = new HashSet<>();
	static HashSet hashset = new HashSet<>();
	static ArrayList<Instance> listInstance = new ArrayList();
	static ArrayList<String> list_url = new ArrayList<>();
	ResultSet resultSet = null;
/*read data from the first csv file and write to arff file*/
	public static void run() {
		String csvFile = "D:/mega folder/thesis/data/hotel_Sheet1 (2).csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "@@";
		String[] title = new String[10];
		String[] value;
		try {
			br = new BufferedReader(new FileReader(csvFile));
			int k = 0, j = 0, i = 0, l = 0;
			while ((line = br.readLine()) != null) {
				if (k == 0) {
					title = line.split(cvsSplitBy);
					for (i = 0; i < 6; i++) {
						attributes.add(new Attribute(title[i],
								(FastVector) (null)));
						atts.addElement(attributes.get(i));
					}
					for (i = 6; i < 12; i++) {
						attributes.add(new Attribute(title[i]));
						atts.addElement(attributes.get(i));
					}
					FastVector myNomVals = new FastVector();
					myNomVals.addElement("0");
					myNomVals.addElement("1");
					attributes.add(new Attribute("unique", myNomVals));
					atts.addElement(attributes.get(12));
					data = new Instances("MyRelation", atts, 0);

				} else {
					vals = new double[data.numAttributes()];
					value = line.split(cvsSplitBy);
					for (j = 0; j < 6; j++) {
						vals[j] = data.attribute(j).addStringValue(value[j]);
					}
					for (j = 6; j < 12; j++) {
						vals[j] = Double.parseDouble(value[j]);
					}
					Instance instance = new Instance(1.0, vals);
					data.add(instance);
					listInstance.add(instance);
					hashset.add(data.lastInstance());
					hashset_url1.add(data.lastInstance().stringValue(4));
					hashset_url2.add(data.lastInstance().stringValue(5));
					list_url.add(data.lastInstance().stringValue(4));
				}
				k++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
/*Read data from the second csv file check if there're duplicates and just add the unexisted instances*/
	public static void run1() {
		String csvFile = "D:/mega folder/thesis/data/Sample_0.75-0.9.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "@@";
		String[] title = new String[10];
		String[] value;
		try {
			br = new BufferedReader(new FileReader(csvFile));
			int k = 0, j = 0, i = 0, l = 0;
			while ((line = br.readLine()) != null) {
				if (k == 0) {
					data1 = new Instances("NewRelation", atts, 0);
				} else {
					vals = new double[data.numAttributes()];
					value = line.split(cvsSplitBy);
					for (j = 0; j < 6; j++) {
						vals[j] = data1.attribute(j).addStringValue(value[j]);
					}
					for (j = 6; j < 12; j++) {
						vals[j] = Double.parseDouble(value[j]);
					}
					Instance instance1 = new Instance(1.0, vals);
					data1.add(instance1);
					if (list_url.contains(data1.lastInstance().attribute(4)))
						System.out.println("yes");
					if (listInstance.indexOf(data1.lastInstance()
							.stringValue(4)) == listInstance.indexOf(data1
							.lastInstance().stringValue(5))) {
						System.out.println(listInstance.indexOf(data1
								.lastInstance().attribute(4))
								+ " "
								+ listInstance.indexOf(data1.lastInstance()
										.attribute(5)));
						data1.delete(data1.numInstances() - 1);
					} else {
						System.out.println("no");
					}
				}
				k++;
			}
			System.out.println("number_instances: " + data1.numInstances());
			System.out.println("num_ttr:" + data1.numAttributes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		run();
		run1();
		saveFile(data1, "D:/mega folder/thesis/test_data_addition.arff");
	}
}