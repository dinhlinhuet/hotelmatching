package creatingAdditionData;

import handlingfile.ManipulateFile;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
/*This class add orgirinal features to new data*/
public class BaseAdditionalDataArff extends ManipulateFile{
    static FastVector atts = new FastVector();
    static Instances data, data1, data2;
    static double[] vars;
    static ArrayList<Attribute> attributes = new ArrayList<>();
    static HashSet hashSet = new HashSet();
    static HashSet hashSet1 = new HashSet();
    static HashSet hashSet2 = new HashSet();
    static HashSet hashSet3 = new HashSet();
    static ArrayList<String> list_code = new ArrayList();

    /* Because there're 2 dataset file former then this class extract 6800 instances which independent with
    subdataset1(3042 instances) from dataset1(9800 instances) */
    public static void readDataset1() {
        ArffLoader loader = new ArffLoader();
        try {
            loader.setFile(new File("D:/mega folder/thesis/data/test_data_sub1.1.arff"));
            data = loader.getDataSet();
            int k = 0;
            while (k < data.numInstances()) {
                String url = data.instance(k).stringValue(4) + data.instance(k).stringValue(5);
                double gelocate = data.instance(k).value(8) + data.instance(k).value(9);
                String url1 = url + gelocate;
                hashSet2.add(url1.hashCode());
                if(hashSet1.add(url1));
                list_code.add(url1);
                k++;
            }
            System.out.println("has1:" + hashSet1.size());
            System.out.println(data.numInstances());
            System.out.println("----------------");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void extractData() {
        ArffLoader loader = new ArffLoader();
        try {
            loader.setFile(new File("D:mega folder/thesis/data/test_data.arff"));
            data1 = loader.getDataSet();
            int i = 0, k = 0;
            for (i = 0; i < 6; i++) {
                attributes.add(new Attribute(i + "",
                        (FastVector) (null)));
                atts.addElement(attributes.get(i));
            }
            for (i = 6; i < 12; i++) {
                attributes.add(new Attribute(i + ""));
                atts.addElement(attributes.get(i));
            }
            FastVector myNomvar = new FastVector();
            myNomvar.addElement("0");
            myNomvar.addElement("1");
            attributes.add(new Attribute(data1.attribute(12).name(), myNomvar));
            atts.addElement(attributes.get(12));
            data2 = new Instances("NewRelation", atts, 0);
            int m = 0,dup=0;
            while (k < data1.numInstances()) {
                String url = data1.instance(k).stringValue(4) + data1.instance(k).stringValue(5);
                double gelocate = data1.instance(k).value(8) + data1.instance(k).value(9);
                String url1 = url + gelocate;
                hashSet2.add(url1.hashCode());
                vars = new double[13];
                if(!list_code.contains(url1)){
                    list_code.add(url1);
                    hashSet1.add(url1);
                    for (i = 0; i < 6; i++) {
                        vars[i] = data2.attribute(i).addStringValue(data1.instance(k).stringValue(i));
                    }
                    for (i = 6; i < 13; i++) {
                        vars[i] = data1.instance(k).value(i);
                    }
                    Instance instance = new Instance(1.0, vars);
                    data2.add(instance);
                    m++;
                }else {
                    dup++;
                }
                k++;
            }
            System.out.println("add" + m);
            System.out.println("size:" + hashSet1.size());//main
            System.out.println("hashcode"+hashSet2.size());
            System.out.println("dup:"+dup);
            System.out.println("SIZE: " + data2.numInstances());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        readDataset1();
        extractData();
        String x = "1.3";
        saveFile(data2, "D:/mega folder/thesis/data/test_data" + x
                + ".arff");
    }
}
