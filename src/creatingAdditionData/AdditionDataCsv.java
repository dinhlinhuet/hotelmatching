package creatingAdditionData;

import handlingfile.ManipulateFile;
import info.debatty.java.stringsimilarity.Jaccard;
import info.debatty.java.stringsimilarity.Levenshtein;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;

import measures.KmDistance;
import preprocessdata.RemoveStopword;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/*Add additional data by write to file arff after add all new features to new data*/
public class AdditionDataCsv extends ManipulateFile{
	static FastVector atts = new FastVector();;
	static Instances data1;
	static double[] vals;
	static ArrayList<Attribute> attributes = new ArrayList<>();
	static RemoveStopword removeStopword = new RemoveStopword();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		removeStopword.readStopWords("D:mega folder/thesis/stopword.txt");
		KmDistance hav  = new KmDistance();
		Levenshtein l = new Levenshtein();
		Jaccard jc = new Jaccard();
		ArffLoader loader = new ArffLoader();
		 try {
			 loader.setFile(new File("D:mega folder/thesis/data/test_data_sub1.arff"));
			 Instances data= loader.getDataSet();
			 int i=0,k=0;
			 attributes.add(new Attribute("Levenshtein_name"));
			 attributes.add(new Attribute("Levenshtein_address"));
			 attributes.add(new Attribute("Jaccard_name"));
			 attributes.add(new Attribute("Jaccard_address"));
			 attributes.add(new Attribute("Levenshtein_name1"));
			 attributes.add(new Attribute("Levenshtein_address1"));
			 attributes.add(new Attribute("Jaccard_name1"));
			 attributes.add(new Attribute("Jaccard_address1"));
			 attributes.add(new Attribute("star"));
			 attributes.add(new Attribute("geolocation"));
			 attributes.add(new Attribute("subtract_lat"));
			 attributes.add(new Attribute("subtract_long"));
			 FastVector myNomVals = new FastVector();
				myNomVals.addElement("0");
				myNomVals.addElement("1");
				attributes.add(new Attribute("unique",myNomVals));
			 for(k=0;k<13;k++){
					atts.addElement(attributes.get(k));
			 }
			 data1 = new Instances("Relation", atts, 0);
			 i=0;
			 while(i<data.numInstances() ){
				 String hotel1 = null,hotel2 = null;
				 String hotel3 =null,hotel4= null;
				 vals = new double[data1.numAttributes()];
				 int j=0;
				 for (j = 0; j < 2; j++) {
					 hotel1= Normalizer.normalize((data.instance(i).stringValue(2*j)), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
					 hotel2= Normalizer.normalize((data.instance(i).stringValue(2*j+1)), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
					 vals[j]= l.distance(hotel1,hotel2 );
					 vals[j+2] = jc.similarity(hotel1, hotel2);
					 System.out.println(hotel1);
					 System.out.println(hotel3);
					 vals[j+4]= l.distance(hotel3,hotel4);
					 vals[j+6] = jc.similarity(hotel3, hotel4);
				 }
				 vals[j+6]= Math.abs(data.instance(i).value(6)-data.instance(i).value(7));
				 vals[j+7]= hav.haversine(data.instance(i).value(8), data.instance(i).value(10),data.instance(i).value(9)
						 ,data.instance(i).value(11));
				 vals[j+8] = Math.abs(data.instance(i).value(8) - data.instance(i).value(9));
				 vals[j+9] = Math.abs(data.instance(i).value(10) - data.instance(i).value(11));
				 
				 vals[j+10]= data.instance(i).value(12);
				 Instance instance = new Instance(1.0, vals);
				 System.out.println(instance);
				 data1.add(instance);
				 i++;
			 }
		} catch (IOException e) {
			e.printStackTrace();
		}
		String x="_sub7";
		saveFile(data1, "D:mega folder/thesis/data/test_data"+x+".arff" );
	}

}
