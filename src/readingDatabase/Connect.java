package readingdatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Connect {

	/**
	 * @param args
	 */
	private static Connection connect = null;
	private static Statement statement=null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet=null;
	
	 public static ResultSet readDataBase() throws Exception {
		   // try {
		      // This will load the MySQL driver, each DB has its own driver
		      Class.forName("com.mysql.jdbc.Driver");
		      // Setup the connection with the DB
		      connect = DriverManager.getConnection(
		    		  "jdbc:mysql://127.0.0.1:3307/mydb", "root", "1234");
		      statement = connect.createStatement();
		      // Result set get the result of the SQL query
//		      String abc = 
//				"DELIMITER$$ create trigger checkRent after insert on Rental for each row  begin update inventory set is_available = false where inventory_id = new.inventory_id end$$ ";
		//String sql2 = "DELIMITER ;";"
		  //    resultSet = statement.executeQuery(abc);
//		      resultSet = statement
//		          .executeQuery("select * from film join film_actor on film.film_id= film_actor.film_id " +
//		          		"where film_actor.actor_id=1");
		      //writeResultSet(resultSet);

		      // PreparedStatements can use variables and are more efficient
//		      preparedStatement = connect
//		          .prepareStatement("insert into  feedback.comments values (default, ?, ?, ?, ? , ?, ?)");
//		      // "myuser, webpage, datum, summery, COMMENTS from feedback.comments");
//		      // Parameters start with 1
//		      preparedStatement.setString(1, "Test");
//		      preparedStatement.setString(2, "TestEmail");
//		      preparedStatement.setString(3, "TestWebpage");
//		      preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
//		      preparedStatement.setString(5, "TestSummary");
//		      preparedStatement.setString(6, "TestComment");
//		      preparedStatement.executeUpdate();
//
//		      preparedStatement = connect
//		          .prepareStatement("SELECT myuser, webpage, datum, summery, COMMENTS from feedback.comments");
//		      resultSet = preparedStatement.executeQuery();
//		      writeResultSet(resultSet);
//
//		      // Remove again the insert comment
//		      preparedStatement = connect
//		      .prepareStatement("delete from feedback.comments where myuser= ? ; ");
//		      preparedStatement.setString(1, "Test");
//		      preparedStatement.executeUpdate();
//		      
//		      resultSet = statement
//		      .executeQuery("select * from ");
		      String readDb = "select * from agency";
		      resultSet = statement.executeQuery(readDb);
		      writeResultSet(resultSet);
		     // writeMetaData(resultSet);
		     // close();
//		    } catch (SQLException e) {
//		    	System.out.println("-- Rolling back changes --"); 
//		    	//connect.rollback(); // Rollback to the last commit. 
//		    } finally {
//		      close();
//		    }
			return resultSet;

		  }

		  private static void writeMetaData(ResultSet resultSet) throws SQLException {
		    //   Now get some metadata from the database
		    // Result set get the result of the SQL query
		    
		    System.out.println("The columns in the table are: ");
		    
		    System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
		    for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
		      System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
		    }
		    close();
		  }

		  private static void writeResultSet(ResultSet resultSet) throws SQLException {
		    // ResultSet is initially before the first data set
		    while (resultSet.next()) {
	
		    }
		  }

		  // You need to close the resultSet
		  private static void close() {
		    try {
		      if (resultSet != null) {
		        resultSet.close();
		      }

		      if (statement != null) {
		        statement.close();
		      }

		      if (connect != null) {
		        connect.close();
		      }
		    } catch (Exception e) {

		    }
		  }

		 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			readDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
