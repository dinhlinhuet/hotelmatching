package preprocessdata;

/**
 * Created by admin on 3/15/2016.
 */
public class NormalizeFeatures {
    public String normalize(String input){
        input = input.replaceAll("&#\\d+;","");
        input = input.replaceAll("’","");
        input = input.replaceAll("\'","");
        return input;
    }

    public static void main(String[] args) {
        NormalizeFeatures normalizeFeatures = new NormalizeFeatures();
        System.out.println(normalizeFeatures.normalize("Sai Gon&#39;s Book Hotel"));
        System.out.println(normalizeFeatures.normalize("Nathalie’s Nhan Hoa Resort"));
    }
}
