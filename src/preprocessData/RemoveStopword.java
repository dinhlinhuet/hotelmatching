package preprocessdata;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hotel.Hotel;
import org.apache.commons.lang.WordUtils;

public class RemoveStopword {
    public ArrayList<String> stopWords;
    public Hotel hotel1 = new Hotel(), hotel2 = new Hotel();
    public NormalizeFeatures normalizeFeatures = new NormalizeFeatures();
    public String[] stopWord3 = {"lots", "plot", "slot", "lot", "group", "to", "km", "ap", "road", "xa", "tinh\\s*lo",
            "khu", "kp", "block", "k", ","};
    private Hotel[] removedHotel = {hotel1, hotel2};

    public RemoveStopword() {
        stopWords = new ArrayList<>();
    }
/*Find the dupplicated text*/
    public String findingDuplicateWords(String textLine) {
        int length = textLine.length();
        String pattern = "(.*?)(.{10,})\\2";
        if (textLine != "") {
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(textLine);
            if (m.find()) {
                textLine = textLine.substring(0, m.end(2));
            } else {
//                System.out.println("NO MATCH");
            }
        }
        return textLine;
    }
/*Check whether text is hotel name or address and trim text and finally remove stopword*/
    public Hotel[] removeStopWords(String hotel1Name, String hotel2Name, int flag) {
        hotel1Name = WordUtils.uncapitalize(hotel1Name);
        hotel2Name = WordUtils.uncapitalize(hotel2Name);
        if(flag == 0){
            hotel1.trimmedHotelName = findStopWords(hotel1Name, flag);
            if(hotel1.trimmedHotelName.split("\\s").length==1)// avoid remove entire hotel name
                hotel1.trimmedHotelName = hotel1Name;
            hotel2.trimmedHotelName = findStopWords(hotel2Name, flag);
            if(hotel2.trimmedHotelName.split("\\s").length==1)
                hotel2.trimmedHotelName = hotel2Name;
        }
        if (flag == 1) {// hotel name
            hotel1.trimmedHotelName = findStopWords(hotel1Name, flag);
            hotel2.trimmedHotelName = findStopWords(hotel2Name, flag);
            for(String compareWord: hotel1.comparativeWord){
                for (String compareWord1: hotel2.comparativeWord) {
                    if(compareStopWord(compareWord,compareWord1) ==0) {
                        hotel1.trimmedHotelName = hotel1Name;
                        hotel2.trimmedHotelName = hotel2Name;
                    }
                }
            }
        }
        if (flag == 2) {// hotel address
            hotel1.trimmedHotelAddress = findStopWords(hotel1Name, flag);
            hotel2.trimmedHotelAddress = findStopWords(hotel2Name, flag);
        }
        return removedHotel;
    }
/*Check whether we should remove stopword in hotel name*/
    public int compareStopWord(String hotel1, String hotel2) {
        int check = 0;
        ArrayList<ArrayList<String>> list_keywords = new ArrayList();
        list_keywords.add(new ArrayList<>(Arrays.asList("khách sạn", "khach san", "hotel", "hotel & residence",
                "hotel & spa")));
        list_keywords.add(new ArrayList<>(Arrays.asList("khu nghỉ dưỡng", "khu nghi duong", "resort", "resort & spa",
                "resort and spa", "residences & resort", "resort & golf", "resort & residences",
                "khu nghỉ dưỡng và spa", "khu nghi duong va spa")));
        list_keywords.add(new ArrayList<>(Arrays.asList("biệt thự", "biet thu", "villa", "villas", "khu biệt thự",
                "villa area", "khu biet thu")));
        list_keywords.add(new ArrayList<>(Arrays.asList("guesthouse", "guest house", "hostel")));
        list_keywords.add(new ArrayList<>(Arrays.asList("cruise", "cruises", "cruiser", "du thuyền")));
        list_keywords.add(new ArrayList<>(Arrays.asList("homestay")));
        for (ArrayList<String> kewword: list_keywords) {
            if(kewword.contains(hotel1)&&kewword.contains(hotel2)) {
                check = 1;
                break;
            }
        }
        if(hotel1.equals(hotel2)){
            check = 1;
        }
        return check;
    }
/*Read stopwords from file*/
    public ArrayList<String> readStopWords(String stopWordsFilename) {
        try {
            Scanner stopWordsFile = new Scanner(new File(stopWordsFilename));
            String stopword = null;
            String listTemp[];
            while (stopWordsFile.hasNextLine()) {
                stopword = stopWordsFile.nextLine();
                listTemp = stopword.split(",\\s");
                for (String word : listTemp)
                    stopWords.add(word);
            }
            stopWordsFile.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        return stopWords;
    }
/*Remove bracket, unwanted words in hotel address*/
    public String normalizeWords(String text) {
        if (text.contains("title"))
            text = text.substring(0, text.indexOf("title")).trim();
        Pattern p = Pattern.compile("(\\(.*\\))");
        Matcher m = p.matcher(text);
        if (m.find()) {
            text = text.replace(m.group(1), "");
        }
        return text;
    }
/*Finding the way to remove stopwords*/
    public String findStopWords(String textLine, int flag_check) {
        textLine = normalizeWords(textLine);
        textLine = normalizeFeatures.normalize(textLine);
        String result = textLine;
        int check = 0;
        String temp = "";
        boolean found = false;
        for (int i = 0; i < stopWords.size(); i++) {
            if (flag_check == 2 && i == 174) {// mark position where to remove duplicated address
                if ((result == null) || (result == ""))
                    System.out.println("");
                else if (result != "")
                    result = findingDuplicateWords(result);

            }
            temp = stopWords.get(i);
            found = Pattern.matches("(^.*" + temp + "[\\W].*$)|(.*" + temp + "$)", result);
            if (found) {
                if (i == 0 || i == 1) {
                    result = result.substring(0, result.indexOf(temp))
                            .trim();
                } else {
                    if(flag_check ==1)
                        hotel1.comparativeWord.add(temp);
                    if(flag_check ==2)
                        hotel2.comparativeWord.add(temp);
                    // System.out.println("com "+comparativeWord);
                    result = result.replace(temp, "").replaceAll("\\s+", " ")
                            .trim();
                }
            }
        }
//        System.out.println(result);
        return result;
    }

    public static void main(String[] arg) {
        RemoveStopword removeStopword = new RemoveStopword();
        removeStopword
                .readStopWords("D:\\OneDrive\\Documents\\thesis\\stopword.txt");
        removeStopword
                .removeStopWords("muong thanh apartment (level 34) agaf,ho Chi Minh City",
                        "96b/1/8 trần phú, trần phú", 2);
//		removeStopword
//		.removeStopWords("19 Phan Đình Phùng, Hoàn Kiếm District, Chợ Đồng Xuân, Hà Nội, Việt Nam 0000");
        // System.out.println(removeStopword.removeStopWords("17 Nguyen Van Cu, Quận Long Biên, Hà Nội, Việt Nam 0000"));
    }
}
