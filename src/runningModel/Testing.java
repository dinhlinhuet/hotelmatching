package runningmodel;

import addingfeatures.AddFeatures;
import weka.classifiers.Classifier;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;

/**
 * Created by admin on 3/28/2016.
 * This class for loading model and display testing result in console
 */
public class Testing {
    private static final String MODELS_PATH = "D:/mega folder/thesis/model/RandomForest_model/";
    private RandomForest tree;
    private String modelName = "RF-16999-58feats-10folds";

    public static void main(String[] args) {
        new Testing();
    }

    public Testing() {
        try {
            tree = (RandomForest) weka.core.SerializationHelper.read(MODELS_PATH + modelName + ".model");
            Instances test = getNewData();
            // train classifier
            test.setClassIndex(test.numAttributes() - 1);
            Instances labeled = new Instances(test);

            Classifier cls = new RandomForest();
            for (int i = 0; i < test.numInstances(); i++) {
                double clsLabel = tree.classifyInstance(test.instance(i));
                labeled.instance(i).setClassValue(clsLabel);
                String prediction = test.instance(i).classAttribute().value((int) clsLabel);
                System.out.println("actual "+ test.instance(i).classValue()+
                        " prediction " + prediction);
            }
           /* Evaluation eval = new Evaluation(test);
            eval.evaluateModel(tree,test);
//            eval.crossValidateModel(tree, test, 10, new Random(1));
            FastVector predictions = eval.predictions();
            for (int i = 0, trainDataSize = test.numInstances(); i < trainDataSize; i++) {
                Instance instance = test.instance(i);
                Prediction prediction = (Prediction) predictions.elementAt(i);

                if (prediction.actual() != prediction.predicted()) {
                    System.out.println(prediction.actual()+" diff "+ prediction.predicted());
                    System.out.println(i);
                    System.out.println(instance);
                }
            }*/
            /*System.out.println(eval.toSummaryString("\nResults\n======\n", false));
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter("D:mega folder/thesis/data/labeled.arff"));
            writer.write(labeled.toString());
            writer.newLine();
            writer.flush();
            writer.close();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*Load data to test*/
    private Instances getNewData() {
        AddFeatures addFeatures = new AddFeatures();
        Instances data = addFeatures.addingFeatures("D:mega folder/thesis/data/extract_test.arff");
        return data;
    }
}
