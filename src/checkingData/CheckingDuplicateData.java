package checkingdata;

import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by admin on 3/9/2016.
 */
public class CheckingDuplicateData {
    static ArffLoader loader = new ArffLoader();
    private static Instances data,data1;
    private static HashSet hashset= new HashSet();
    private static HashSet hashset1= new HashSet();
    public static void checkingDuplicateRecord(){
        try {
            loader.setFile(new File("D:\\mega folder\\thesis\\data\\test_data_sub1.1.arff"));
            data = loader.getDataSet();
            int k=0,j=0,dup=0;
            while (k < data.numInstances()) {
                String url = data.instance(k).stringValue(4)+ data.instance(k).stringValue(5);
                double gelocate= data.instance(k).value(8)+ data.instance(k).value(9);
                String ur1= url+ gelocate;
                if(hashset1.add(ur1)) {
                    j++;
                }else {
                    dup++;
                }
                k++;
            }
            System.out.println("dup_array"+hashset1.size());
            System.out.println("duplicate "+dup);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String []args){
        checkingDuplicateRecord();
    }
}
