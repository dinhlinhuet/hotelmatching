package measures;

public class KmDistance {
    public static final double R = 6372.8; // In kilometers

    public static double haversine(double latitude1, double lontitude1, double latitude2, double lontitude2) {
        double dLatitude = Math.toRadians(latitude2 - latitude1);
        double dLongtitude = Math.toRadians(lontitude2 - lontitude1);
        latitude1 = Math.toRadians(latitude1);
        latitude2 = Math.toRadians(latitude2);

        double angle = Math.pow(Math.sin(dLatitude / 2), 2) + Math.pow(Math.sin(dLongtitude / 2), 2) *
                Math.cos(latitude1) * Math.cos(latitude2);
        double c = 2 * Math.asin(Math.sqrt(angle));
        return R * c;
    }

    public static void main(String[] args) {
        System.out.println(haversine(12.23694327, 109.1966343, 12.23672079, 109.1968194));
    }
}