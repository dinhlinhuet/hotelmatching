package measures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by admin on 3/7/2016.
 */
public class HeadOfStringRules {
    int j;
    static ArrayList<Double> value_of_feat = new ArrayList();
    static double[] value_of_feats = new double[45];
    static HeadOfStringRules hr = new HeadOfStringRules();
    static int list1[] = {32,8};// <>=
    static int list2[] = {10, 12, 14, 23, 24, 25, 26, 27, 36};//===
    static int list3[] = {6, 41, 42, 43, 44, 45};// immediate
    static ArrayList arrayList1 = new ArrayList(Arrays.asList(list1));
    static ArrayList arrayList2 = new ArrayList(Arrays.asList(list2));
    static ArrayList arrayList3 = new ArrayList(Arrays.asList(list3));
/*Assign regular expression and create head of string features*/
    public double[] createRulesHeadofString(String address1, String address2) {
        String reg[] = new String[45];
        reg[0] = "(\\d+)([a-z])\\s*-\\s*(\\d+)([a-z])(([a-z]|\\s*)*),";
        reg[1] = "(\\d*)([a-z]*)\\s*-*\\s*(\\d+)(([a-z]|\\s*)*),";//68 68c - 70 32a - 34 Bùi 34
        reg[2] = "(\\d+)([a-z])\\s*-*/*\\s*(\\d+)([a-z])(\\d*)(([a-z]|\\s*)*),";//8a/8d2  8A/8D, 2 Thai
        reg[3] = "(\\d+)([a-z]*)-(\\d+)-*(\\d+)([a-z]*)(([a-z]|\\s*)*),";//135a-137-137a  135-137
        reg[4] = "(\\d*)\\s*([a-z])(\\d+)-\\s*([a-z])(\\d*)(([a-z]|\\s*)*),";//14m1-n1 14 M1- N
        reg[5] = "(\\d+)\\s*([a-z])\\s*-*\\s*([a-z])(([a-z]|\\s*)*),";//252 B-c 252b-c
        reg[6] = "(\\d+)([a-z])\\s*/\\s*(\\d+)/*(\\d*)/*([a-z]*)(([a-z]|\\s*)*),";//96a/6/7A 96a/6==
        reg[7] = "([a-z]\\d+/[a-z]\\d+)(([a-z]|\\s*)*),";//C1/d6  C1/d6 New
        reg[8] = "(\\d+)[a-z]*-*(\\d*)-(\\d+)(([a-z]|\\s*)*),";//52b-62-64 ,56-58
        reg[9] = "(\\d+)\\s*-\\s*(\\d*[a-z]*)(\\d+)(([a-z]|\\s*)*),";//39-41 Thủ 39-39a- 41 Thủ
        reg[10] = "(\\d+)([a-z])\\s*/*\\s*(\\d*)(([a-z]|\\s*)*),";//96a/6 ==
        reg[11] = "0*(\\d+)\\s*-*/*,*\\s*0*(\\d+)(([a-z]|\\s*)*),";//06-08 Phan Boi Chau  6-8 Phan Bội Châu
        reg[12] = "(\\d+)/*-*\\+*\\s*(\\d+)(([a-z]|\\s*)*),";//21/42 21 Lane 42 90 + 92 ( Lô ==
        reg[13] = "(\\d+)([a-z]*)((\\s*-\\s*\\d+[a-z]*)*)\\s*-\\s*(\\d+)([a-z]*)(([a-z]|\\s*)*),";//195-197-199 195-199 117 - 123  117-119-121-123
        reg[14] = "(\\d+)\\s*-*([a-z])(([a-z]|\\s*)*),";//254d 254 D ==
        reg[15] = "(\\d+)/*(\\d*)/*(\\d*)(([a-z]|\\s*)*),";//43/45/47  43 Gia Ngư,
        reg[16] = "(\\d*)\\s*(\\d+/*-*\\d+)(([a-z]|\\s*)*),";//288 1/4 Street Số 240 Đường 1-4, Road 1-4, Cat Ba Town
        reg[17] = "(\\d+/\\d+)\\s*([a-z])(([a-z]|\\s*)*),";// 246/2b Nguyễn  246/2 B
        reg[18] = "(\\d+)\\s*-\\s*(\\d+)[a-z]*(([a-z]|\\s*)*),";//40-42b 40 - 42 Lò
        reg[19] = "0*(\\d*)-*0*(\\d+)-0*(\\d+)(([a-z]|\\s*)*),";//9-11 Nguyễn Đức Cảnh,  07-09-11, Đường
        reg[20] = "(\\d+)(/*|,*|-*)\\s*(\\d+)(([a-z]|\\s*)*),";//2/86 Duy Tan, No. 2, Lane 86, Duy Tan Street
        reg[21] = "([a-z]\\d+)\\s*-\\s*(\\d+)(([a-z]|\\s*)*),";//Z72 - 77 Z72-77
        reg[22] = "([a-z])(\\d+)-*,*\\1*(\\d+)-*,*\\1*(\\d+)(([a-z]|\\s*)*),";//Lô Z5,z6,z7  Lots Z5-6-7
        reg[23] = "([a-z])\\s*(\\d+)(([a-z]|\\s*)*),";//A30 A 30,  ==
        reg[24] = "(\\d+)([a-z]*)(\\d*)(([a-z]|\\s*)*),";//99C1 99 ==
        reg[25] = "(\\d+)\\s*-*\\s*(\\d*)\\s*-*\\s*(\\d*)(([a-z]|\\s*)*),"; //29-30-34 Số 34 ==
        reg[26] = "(\\d+)\\s*-*\\s*(\\d*)(([a-z]|\\s*)*),";//12-14 Tran 12 ==
        reg[27] = "(\\d+)([a-z]+)(([a-z]|\\s*)*),";//50a 3a ==
        reg[28] = "(\\d+)([a-z])((\\s*-\\s*[a-z])*)(([a-z]|\\s*)*),";//10a-b-c 10a
        reg[29] = "\\b0*(\\d+)/*(\\d*)(([a-z]|\\s*)*),";//2/36 36 //== not contain
        reg[30] = "(([a-z]|\\s*)*),";//Thôn An Sơn An Son Hamlet
        reg[31] = "(\\d+)-*(\\d*)\\s*/*\\s*(\\d+)(([a-z]|\\s*)*),";//10/168 Hao Số 10-12, Ngõ168
        reg[32] = "\\b0*(\\d+)-*,*0*(\\d*)(([a-z]|\\s*)*),";//18-20, 19
        reg[33] = "\\s+(ii|2)(([a-z]|\\s*)*),";//Gia II  R4-70 Hưng Gia 2
        reg[34] = "(group\\s*(\\d+)|((\\d+)\\s*group))(([a-z]|\\s*)*),";
        reg[35] = "(lot|plot|slot|to\\s)\\s*(\\w+)(([a-z]|\\s*)*),";
        reg[36] = "(\\d+),\\s+(([a-z]|\\s*)*),";//==
        reg[37] = "(\\d+)([a-z])(\\d+)/*\\d*(([a-z]|\\s*)*),";
        reg[38] = "km\\s*(\\d+)(([a-z]|\\s*)*),";
        reg[39] = "ap\\s*(\\d+)(([a-z]|\\s*)*),";

        //----------------------------------
        reg[40] = "(\\d+)([a-z]*)(([a-z]|\\s*)*),";//check
//        reg[41] = "(duong|road)\\s*(\\d+)\\s*-*/*\\s*(\\d+)(([a-z]|\\s*)*),";//0
//        reg[42] = "xa(([a-z]|\\s*)*),";//-1
        reg[41] = "(^\\d*/*\\d*)(([a-z]|\\s*)*),";//0
        reg[42] = "tinh\\s*lo\\s*(\\d+)(([a-z]|\\s*)*),";
        reg[43] = "(lot|lots|plot|slot|lo|to)\\s*(\\w+)\\s+(([a-z]|\\s*)*),";//0
        reg[44] = "(group|k|khu|kp|block)\\s*(\\w+)\\s+(([a-z]|\\s*)*),";//0

        double checkin = 0;
        if (address1.equals("") || address2.equals(""))
            checkin = -1;
        int limit = reg.length;
/*For each regular expression, create one feature and assign 0,-1,1 to its value*/
        for (j = 0; j < limit; j++) {
            String regex = reg[j];
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(address1);
            Matcher m1 = p.matcher(address2);
            int compare_num_group;
/*Check if hotel address match each regex*/
            if (m.find() && m1.find()) {
                int num1 = 0, num2 = 0;
                int count_gr = m.groupCount() - 1;
                int count_gr1 = m1.groupCount() - 1;
                String temp = null, temp1 = null;
/*Count number of groups of these head of string base on regex*/
                for (int i = 1; i <= count_gr; i++) {
                    temp = m.group(i);
                    if (temp == null || temp.isEmpty())
                        continue;
                    num1++;
                }
                for (int i = 1; i <= count_gr1; i++) {
                    temp1 = m1.group(i);
                    if (temp1 == null || temp1.isEmpty())
                        continue;
                    num2++;
                }
/*Process comparing number of groups base on regex of two head of hotel address*/
                if ((num1 == num2) && (num1 == 2))
                    compare_num_group = 0;
                else {
                    if (num1 != num2 && (num1 == 2 || num2 == 2))
                        if (num1 > num2)
                            compare_num_group = 1;
                        else
                            compare_num_group = 2;
                    else if (num1 > num2)
                        compare_num_group = 3;
                    else
                        compare_num_group = 4;
                }
/*Check and generate all the cases features value should be*/
                    if (address2.contains(m.group(count_gr)) || address1.contains(m1.group(count_gr1)))
                        switch (j) {
                            case 8:
                            case 32://compare
                                switch (compare_num_group) {
                                    case 0:
                                        if (Integer.valueOf(m1.group(1)) == Integer.valueOf(m.group(1)))
                                            checkin = 1;
                                        else
                                            checkin = -1;
                                        break;
                                    case 1:
                                        if(!m.group(count_gr-1).isEmpty())
                                            if ((Integer.valueOf(m1.group(1)) <= Integer.valueOf(m.group(count_gr - 1)))
                                                    && Integer.valueOf(m1.group(1)) >= Integer.valueOf(m.group(1)))
                                                checkin = 1;
                                            else {
                                                checkin = -1;
                                            }
                                        else
                                        checkin = 0;
                                        break;
                                    case 2:
                                        if(!m1.group(count_gr1-1).isEmpty())
                                            if ((Integer.valueOf(m.group(1)) <= Integer.valueOf(m1.group(count_gr1 - 1)))
                                                    && Integer.valueOf(m.group(1)) >= Integer.valueOf(m1.group(1))) {
                                                checkin = 1;
                                            } else {
                                                checkin = -1;
                                            }
                                        else
                                            checkin = 0;
                                        break;
                                    case 3:
                                        if(!m.group(count_gr-1).isEmpty() && !m1.group(count_gr1-1).isEmpty())
                                            if ((Integer.valueOf(m1.group(count_gr1 - 1)) <= Integer.valueOf(m.group(count_gr - 1)))
                                                    && Integer.valueOf(m1.group(1)) >= Integer.valueOf(m.group(1)))
                                                checkin = 1;
                                            else {
                                                checkin = -1;
                                            }
                                        else
                                            checkin = 0;
                                        break;
                                    case 4:
                                        if(!m.group(count_gr -1).isEmpty()&& !m1.group(count_gr1-1).isEmpty())
                                            if ((Integer.valueOf(m.group(count_gr - 1)) <= Integer.valueOf(m1.group(count_gr1 - 1)))
                                                    && Integer.valueOf(m.group(1)) >= Integer.valueOf(m1.group(1))) {
                                                checkin = 1;
                                            } else {
                                                checkin = -1;
                                            }
                                        else
                                            checkin = 0;
                                        break;
                                }
                                break;
                            case 10:
                            case 12:
                            case 14:
                            case 23:
                            case 24:
                            case 26:
                            case 27:
                            case 36:
                                switch (compare_num_group) {
                                    case 1:
                                    case 3:
                                        for (int k = 1; k <= count_gr1; k++) {
                                            if (!m1.group(k).isEmpty())
                                                if (m.group(k).trim().equals(m1.group(k).trim()))
                                                    checkin = 1;
                                                else {
                                                    checkin = -1;
                                                    break;
                                                }
                                        }
                                    case 2:
                                    case 4:
                                        for (int k = 1; k <= count_gr; k++) {
                                            if (m.group(k).length() != 0)
                                                if (m.group(k).trim().equals(m1.group(k).trim()))
                                                    checkin = 1;
                                                else {
                                                    checkin = -1;
                                                    break;
                                                }
                                        }
                                }
                                break;
                            case 6:
                            case 41:
                            case 42:
                            case 43:
                            case 44:
                                if ((!m1.group(count_gr1).isEmpty()) && (!m.group(count_gr).isEmpty())) {
                                    int k = 1;
                                    if (m.group(count_gr).trim().equals(m1.group(count_gr1).trim())) {
                                        while (!m.group(k).isEmpty() || !m1.group(k).isEmpty()) {
                                            if (m.group(k).trim().equals(m1.group(k).trim()))
                                                checkin = 0;
                                            k++;
                                        }
                                        if (!m.group(k).isEmpty() || !m1.group(k).isEmpty())
                                            checkin = 0;
                                        else
                                            checkin = 1;
                                    } else {
                                        checkin = -1;
                                    }
                                }
                                break;
                            case 40:
                                if (m.group(count_gr).trim().equals(m1.group(count_gr1).trim())) {
                                    if (m.group(1).equals(m1.group(1))) {
                                        if (m.group(2).equals(m1.group(2)))
                                            checkin = 1;
                                        else
                                            checkin = 0;
                                    } else {
                                        checkin = -1;
                                    }
                                } else {
                                    checkin = -1;
                                }
                                break;
                            default:
                                switch (compare_num_group) {
                                    case 0:
                                    case 1:
                                    case 3:// contain
                                        for (int k = 1; k <= count_gr1; k++) {
                                            if (m1.group(k) != null && !m1.group(k).trim().isEmpty())
                                                if (address1.contains(m1.group(k).trim())) {
                                                    for (int t = 1; t <= count_gr; t++) {
                                                        if (m.group(t) != null && m1.group(t) != null)
                                                            if (m.group(t).trim().equals(m1.group(k).trim())) {
                                                                checkin = 1;
                                                                break;
                                                            } else {
                                                                checkin = -1;
                                                            }
                                                    }
                                                } else {
                                                    checkin = -1;
                                                    break;
                                                }
                                        }
                                        break;
                                    case 2:
                                    case 4:
                                        for (int k = 1; k <= count_gr; k++) {
                                            if (m.group(k) != null && !m.group(k).trim().isEmpty())
                                                if (address2.contains(m.group(k).trim()))
                                                    for (int t = 1; t <= count_gr; t++) {
                                                        if (m.group(t) != null && m1.group(t) != null)
                                                            if (m.group(t).trim().equals(m1.group(k).trim())) {
                                                                checkin = 1;
                                                                break;
                                                            } else {
                                                                checkin = -1;
                                                            }
                                                    }
                                                else {
                                                    checkin = -1;
                                                    break;
                                                }
                                        }
                                        break;
                                }
                                break;
                        }
                    else {
                        checkin = -1;
                    }
            } else {
                checkin = 0;
            }
            if (checkin == -1) {
                value_of_feat.add((double) -1);
                value_of_feats[j] = -1;
            }
            if (checkin == 0) {
                value_of_feat.add((double) 0);
                value_of_feats[j] = 0;
            }
            if (checkin == 1) {
                value_of_feat.add((double) 1);
                value_of_feats[j] = 1;
            }
        }
        return value_of_feats;
    }
/*Test these invented features*/
    public static void main(String[] args) {
//        hr.createRulesHeadofString("138 nguyen dinh chieu,",
//                "138a nguyen dinh chieu, xa ,");
//        createRulesHeadofString("252 b-c, Le Thanh Ton, Ben Thanh, Quận 1, Tp. Hồ Chí Minh, Việt Nam",
//                "252b-c Lê Thánh Tôn, Q. 1, Tp. Hồ Chí Minh");
//                        "252 Bc Le Thanh Ton Str, Ho Chi Minh City, Việt Nam");
//        hr.createRulesHeadofString("100c/14 tran hung dao, ",
//                "100c/8 tran hung dao, ");
//        hr.createRulesHeadofString("01 tran hung dao, Dương Đông, Phú Quốc, Kiên Giang, Việt Nam",
//                "tran hung dao , Duong Dong Beach, Dương Đông, Việt Nam");
//        hr.createRulesHeadofString("12-14 tran phu, Nha Trang, Việt Nam",
//                "12 tran phu,nha Trang, Việt Nam");
//        hr.createRulesHeadofString("4 ton dan, Nha Trang, Việt Nam",
//                "04 ton dan, Phường Lộc Thọ, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("39-41 thu khoa huan, P. Bến Thành, Q.1, Quận 1, Hồ Chí Minh, Việt Nam",
//                "39-39a- 41 thu khoa huan , Q. 1, Tp. Hcm" );
//        createRulesHeadofString("50a Truong Quoc Dung. Ward 10, Quận Phú Nhuận, Tp. Hồ Chí Minh, Việt Nam",
//                "3a Võ Văn Tần, Quận 3, Dinh Thống Nhất/ Bảo Tàng Chứng Tích Chiến Tranh, Hồ Chí Minh, Việt Nam");
//        hr.createRulesHeadofString("29-30-34 hang manh, P.hàng Gai, Quận Hoàn Kiếm, Hà Nội",
//                " 34 , hang manh, Quận Hoàn Kiếm, Phố Cổ - Quận Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("96a/6/7a tran phu , Nha Trang, Việt Nam",
//                "96a/6 tran phu, Khánh Hòa, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam 0084");
//        hr.createRulesHeadofString("06-08 phan boi chau , Nha Trang, Việt Nam",
//                "6-8 phan boi chau, Trung Tâm Thành Phố / Chợ Đầm, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("96A/6/7 tran phu, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam",
//                "96A6/6 tran phu, Loc Tho Ward,Nha Trang, Việt Nam");
//        createRulesHeadofString("35 Phố Bát Sứ , Phố Cổ - Quận Hoàn Kiếm, Hà Nội, Việt Nam ",
//                "35 - 37 Bát Sứ, Quận Hoàn Kiếm, Hà Nội.");
//        hr.createRulesHeadofString("99c1 nguyen thien thuat, Nha Trang, Việt Nam ",
//                "99 nguyen thien thuat, Trung Tâm Thành Phố / Nguyễn Thiện Thuật, Nha Trang, Việt Nam");
//          hr.createRulesHeadofString("52/50 hang be , Hoàn Kiếm , Hà Nội ",
//                "so nha 50 , Phố Hàng Bè, Phố Cổ , Thành Phố Hà Nội, Phố Cổ - Quận Hoàn Kiếm, Hà Nội, Việt Nam 0084");
//        hr.createRulesHeadofString("1k - 2k hung vuong, Nha Trang, Khánh Hòa ",
//                "1k-2k hung vuong, Phường Lộc Thọ, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("40-42b lo su, Quận Hòan Kiếm, Quận Hoàn Kiếm - Hồ Hoàn Kiếm, Hà Nội, Việt Nam",
//                "40 - 42 lo su, Quận Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("10a-b-c le thanh ton, Phường Bến Nghé, Quận 1, Hồ Chí Minh (sài Gòn), Việt Nam",
//                "10a le thanh ton, Quân 1, Quận 1 - Khu Người Nhật, Hồ Chí Minh, Việt Nam 70000");
//        hr.createRulesHeadofString("21/170 hoang ngan, Cau Giay District,hanoi",
//                " 170 hoang ngan, Khu Trung Hòa, Quận Cầu Giấy");
//        hr.createRulesHeadofString("135a-137-137a hoang van thu, Nha Trang, Việt Nam",
//                "135-137 hoang van thu, Trung Tâm Thành Phố / Nguyễn Thiện Thuật, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("90 + 92 tran phu, Nha Trang, Việt Nam",
//                "90-92 tran phu, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("10/168 hao nam,dong Da,hanoi, Vietnam ",
//                "10-12 168 , pho hao nam , Trung Tâm Hội Nghị Giảng Võ - Láng Hạ, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("117 - 123 dong khoi, Quận 1, Thành Phố Hồ Chí Minh ",
//                "117-119-121-123 dong khoi, Quận 1, Quận 1 - Đồng Khởi / Nguyễn Huệ, Hồ Chí Minh, Việt ");
//        hr.createRulesHeadofString("32a - 34 bui thi xuan, Quận 1, Hồ Chí Minh (sài Gòn), Việt Nam ",
//                "34 bui thi xuan, Phường Bến Thành, Quận 1 - Chợ Bến Thành, Hồ Chí Minh, Việt Nam");
//        hr.createRulesHeadofString(" 254d thuy khue, Quận Tây Hồ, Hà Nội.",
//                "254 d thuy khue, Quận Tây Hồ, Quận Tây Hồ - Hồ Tây/nghi Tàm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("43/45/47 gia ngu, Quận Hoàn Kiếm, Hà Nội, Việt Nam",
//                "43 gia ngu, Quận Hòan Kiếm, Phố Cổ - Quận Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("195-197-199 hang bong ,Street Hoan Kiem,hanoi",
//                "195-199 hang bong , Nhà Thờ Lớn Hà Nội - Quận Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("18-20  nui ngoc, Biển Thị Trấn Cát Bà, Quần Đảo Cát Bà, Việt Nam",
//                "19 nui ngoc, Cát Bà, Việt Nam");
//        hr.createRulesHeadofString("288 1/4 , Cát Bà, Việt Nam",
//                " 1/4 , Thị trấn Cát Bà, Biển Thị Trấn Cát Bà, Quần đảo Cát Bà, Việt Nam");
//        hr.createRulesHeadofString(" 240 1-4, Cát Bà, Cát Hải, Hải Phòng, Biển Thị Trấn Cát Bà, Quần Đảo Cát Bà, Vi",
//                " 1-4, Cat Ba Town,haiphong, Vietnam");
//        hr.createRulesHeadofString("246/2b nuyen dinh chieu , Hàm Tiến - Mũi Né - Tp Phan Thiết",
//                "246/2 b, Biển Mũi Né, Phan Thiết, Việt Nam");
//        hr.createRulesHeadofString("Tầng 48-60f, Tòa Nhà Keangnam Landmark72, E6, Đường Phạm Hùng,quận Cầu Giấy, Quận",
//                "Tầng 48 -60 ,Tòa Nhà Keangnam Landmark 72; E6, Đường Phạm Hùng, Quận Cầu Giấy ");
//        hr.createRulesHeadofString("5 nguyen thong, Mũi Né - Phan Thiết, Phan Thiết",
//                "Phu Hai, Biển Mũi Né, Phan Thiết, Việt Nam");
//        hr.createRulesHeadofString("9-11 Nguyễn Đức Cảnh, Tp. Buôn Ma Thuột, Đắk Lắk  ",
//                "07-09-11, Đường Nguyễn Đức Cảnh, Thành Phố Buôn Ma Thuột, Trung tâm thành phố Buô");
//        hr.createRulesHeadofString("c1/d6 ,New Cau Giay Residence, Dich Vong Ward, Cau Giay District, Quận Cầu Giấy, Hà Nội, Việt Nam",
//                "c1/d6 ,New Cau Giay Residence, Dich Vong Ward, Cau Giay District, Hà Nội, Việt Nam");
//        createRulesHeadofString("No. 2, Lane 86, Duy Tan Street Cau Giay Districtno. 2, Lane 86, Duy Tan Street Cau Giay District,hanoi, Vietnam ",
//                "2/86 Duy Tan, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("14 m1- n, Đường 38, Quốc Hương, Phường Thảo Điền, Quận 2, Tp. Hồ Chí Minh, Việt Nam",
//                "14m1-n1, Path 38 Of Quoc Huong Street , Ward Thao Dien( The Path Is Next To Thao Dien ");
//        createRulesHeadofString("68c - 70 Hang Bo Street, Quận Hoàn Kiếm, Hà Nội, Việt Nam",
//                "68 Hàng Bồ, Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("11/09 ky dong, Phường 9, Quận 3, Ga Sài Gòn - Kỳ Đồng, Hồ Chí Minh, Việt Nam",
//                "9-11 ky dong , Quận 3, Tp. Hồ Chí Minh, Việt Nam");
//        hr.createRulesHeadofString("01,03 phan chu trinh , Thành Phố Buôn Ma Thuột , Daklak ",
//                " 01-03 phan chu trinh , Buon Ma Thuot, Dak Lak, Buôn Ma Thuột, Việt Nam");
//        hr.createRulesHeadofString("8a/8d2 thai lung, Quận 1, Quận 1 - Khu Người Nhật, Hồ Chí Minh, Việt Nam",
//                "8a/8d, 2 thai van lung, Quận 1, TP. Hồ Chí Minh, Việt Nam");
//        hr.createRulesHeadofString("hung gia ii ,- phu my hung, Quận 9, TP. Hồ Chí Minh, Việt Nam",
//                "r4-80 2,hung gia 2, phu my hung, Quận 7, Triển Lãm Sài Gòn & Tt Hội Nghị/phú ");
//        hr.createRulesHeadofString("k5 nghi tam, 11 Xuan Dieu Road, Quận Tây Hồ, Hà Nội, Việt Nam",
//                "k5 nghi tam, 11 Đường Xuân Diệu, Tây Hồ, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("z72-77 tran hung dao,da Nang,trung Tâm Thành Phố Đà Nẵng",
//                "z72 - 77 tran hung dao, Son Tra, Đà Nẵng, Việt Nam");
//        hr.createRulesHeadofString("lots z5-6-7, tran hung dao , Son Tra District , Đà Nẵng, Việt Nam",
//                "lo z5,z6,z7 tran hung dao, Quận Sơn Trà, Đà Nẵng.");
//        createRulesHeadofString("A30 Tran Hung Dao Street, Son Tra District,, Đà Nẵng, Việt Nam",
//                "A 30, Trần Hưng Đạo,quận Sơn Trà, Đà Nẵng, Việt Nam");
//        createRulesHeadofString("Khu 387 My Khe, Đà Nẵng, Việt Nam",
//                "Lô 4.5 B2.5, Khu 387 Bãi Biễn Mỹ Khê, P.mỹ An, Q.ngũ Hành Sơn, Đà Nẵng, Việt Nam");
//        hr.createRulesHeadofString("21/42 nguyen cong tru, Huế, Việt Nam",
//                "21  42 nguyen cong tru , Trung Tâm Thành Phố Huế, Huế, Việt Nam");
//        createRulesHeadofString("Unit3, Thanh Tay, Cam Chau, Ven Sông Hội An, Hội An, Việt Nam",
//                "Unit 3, Thanh Tay, Cam Chau, Hoi An, Việt Nam");
//        createRulesHeadofString("2/36 Võ Thị Sáu, Trung tâm thành phố Huế, Huế, Việt Nam",
//                "36 Võ Thị Sáu, Huế, Huế, Việt Nam");@@
//        hr.createRulesHeadofString("52b-62-64 pham hong thai , Ho Chi Minh City, Việt Nam",
//                "56-58 pham hong thai, Quận 1, Tp. Hồ Chí Minh, Quận 1 - Chợ Bến Thành, Hồ Chí Minh ");
//        createRulesHeadofString("30 Au Co, Liên Sơn, Việt Nam",
//                "Liên Sơn, Quận Lak, Huyện Lắk, Buôn Ma Thuột, Việt Nam");
//        createRulesHeadofString("Bãi Dâu, Vũng Tàu, Bãi Dứa, Vũng Tàu, Việt Nam",
//        "03-06 Ha Long St., Ward 2, Vung Tau, Việt Na" );
//        hr.createRulesHeadofString("Lot 29-33 Le Van Quy, Đà Nẵng, Việt Nam",
//                "B26-29 Pham Van Dong St., Danang - Vietnam");
//        hr.createRulesHeadofString("20 Trần Phú, Nha Trang, Khánh Hòa",
//                "60 Trần Phú, Đường Biển Trần Phú, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("96b5 tran phu, Nha Trang, Việt Nam",
//                "96b/1/8 tran phu, Đường Biển Trần Phú, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("Lot G Pham Van Dong Street, Da Nang, Việt Nam",
//                "Lot G, Pham Van Dong, Đà Nẵng, Việt Nam");
//        //-------------------------0
//        hr.createRulesHeadofString("102/1 Cong Quynh Street , Quận 1, Tp. Hồ Chí Minh, Việt Nam",
//                "102abc Cong Quynh Street Pham Ngu Lao Ward, District 1");
//        hr.createRulesHeadofString("28/1 Bui Vien, Quận 1, Tp. Hồ Chí Minh, Việt Nam",
//                "28/10 Bùi Viện, Hồ Chí Minh");
//        hr.createRulesHeadofString("Số 1, Phố Lê Thánh Tông, Quận Hoàn Kiếm, Nhà Hát Thành Phố - Quận Hoàn Kiếm, Hà Nội",
//                "Số 1, Phố Tôn Đản, Quận Hoàn Kiếm, Quận Hoàn Kiếm - Hồ Hoàn Kiếm, Hà Nội, Việt Nam");
//        hr.createRulesHeadofString("96b1/4 Trần Phú, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam",
//                "96b4 Trần Phú, Ven Biển / Trung Tâm Đường Trần Phú, Nha Trang, Việt Nam");
//        hr.createRulesHeadofString("77/7 Nguyen Duy Hieu, Cẩm Châu, Hội An, Việt Nam",
//                "77c Nguyen Duy Hieu, Ven Sông Hội An, Hội An, Việt Nam 563824");
//        hr.createRulesHeadofString("Số 211, Đường 1-4, Biển Thị Trấn Cát Bà, Quần Đảo Cát Bà, Việt Nam",
//                "Road 1-4, Cat Ba Town, Haiphong, Việt Nam");
//        hr.createRulesHeadofString("Xã Ninh Thủy, Thị Trấn Ninh Hoà, Vịnh Vân Phong, Nha Trang, Việt Nam",
//                "Xã Ninh Hải, Huyện Ninh Hòa, Khánh Hòa");
//        hr.createRulesHeadofString("55/5 Nguyễn Thiện Thuật, Trung Tâm Thành Phố / Nguyễn Thiện Thuật, Nha Trang, Việt Nam",
//                "Nguyễn Thiện Thuật ,nha Trang ");

    }
}
