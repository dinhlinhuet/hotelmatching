package measures;

import hotel.Hotel;
import measures.CosineSimilarity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DocumentParser {
    private List<String> allTerms = new ArrayList<String>(); //to hold all terms
    public Hotel hotel1, hotel2;
    public String[] parseFiles(String hotelName) {
        String[] tokenizedTerms = hotelName.split("\\s+");   //to get individual terms

        for (String term : tokenizedTerms) {
            if (!allTerms.contains(term)) {  //avoid duplicate entry
                allTerms.add(term);
//                System.out.println(term);
            }
        }
//        System.out.println(tokenizedTerms.length);
//        System.out.println(tokenizedTerms[1]);
        return tokenizedTerms;
    }

    public ArrayList tfCalculator(String[] list_word) {
        ArrayList<Integer> vector = new ArrayList<Integer>();
        int value=0;
        for (String term: allTerms) {
            value =0;
            for (String temp: list_word) {
//                System.out.println(temp);
                if(temp.equals(term))
                    value++;
            }
            vector.add(value);
        }
//        System.out.println(vector);
        return vector;
    }
    public Hotel[] getVector(String hotel1Name, String hotel2Name) {
        hotel1 = new Hotel();
        hotel2 = new Hotel();
        String[] list1 = parseFiles(hotel1Name);
        String[] list2 = parseFiles(hotel2Name);
        hotel1.vector = tfCalculator(list1);
        hotel2.vector = tfCalculator(list2);
        return new Hotel[]{hotel1,hotel2};
    }
}
