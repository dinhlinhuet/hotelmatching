package creatingdata;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import handlingfile.ManipulateFile;
import readingdatabase.Connect;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class BaseDataCreation extends ManipulateFile {
	static FastVector atts = new FastVector();
	static Instances data;
	static double[] vals;
	int i;
	List<String> line_temp = new ArrayList<String>();
	ArrayList<FastVector> list_vector = new ArrayList<>();
	ArrayList<String> nameAttributes = new ArrayList<>();
	static ArrayList<Attribute> attributes = new ArrayList<>();
	ArrayList<Connect> ag = new ArrayList<>();
	static HashSet hashset = new HashSet<>();
	ResultSet resultSet = null;

	public static void addFeatures() {
		String csvFile = "D:/OneDrive/Documents/thesis/data/hotel1.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "@@";
		String[] title = new String[10];
		String[] value;
		try {
			br = new BufferedReader(new FileReader(csvFile));
			int k = 0, j = 0, i = 0,l=0;
			while (((line = br.readLine()) != null)&&k<7132) {
				if (k == 0) {
					title = line.split(cvsSplitBy);
					for (i = 0; i < 6; i++) {
						attributes.add(new Attribute(title[i],
								(FastVector) (null)));
						atts.addElement(attributes.get(i));
					}
					for(i=6;i<12;i++){
						attributes.add(new Attribute(title[i]));
						atts.addElement(attributes.get(i));
					}
					FastVector myNomVals = new FastVector();
					myNomVals.addElement("0");
					myNomVals.addElement("1");
					attributes.add(new Attribute("unique",myNomVals));
					atts.addElement(attributes.get(12));
					data = new Instances("MyRelation", atts, 0);
				} else {
					vals = new double[data.numAttributes()];
					value = line.split(cvsSplitBy);
					for (j = 0; j < 6; j++) {
						vals[j] = data.attribute(j).addStringValue(value[j]);
					}
					for(j=6;j<12;j++){
						vals[j]= Double.parseDouble(value[j]);
					}
					if(value[12].equals("0")){
						vals[12]= data.attribute(12).indexOfValue("0");
						l++;
					}else{
						vals[12]= data.attribute(12).indexOfValue("1");
					}
					Instance instance = new Instance(1.0, vals);
					data.add(instance);
				}
				k++;
			}
			System.out.println("number_instances: " + data.numInstances());
			System.out.println("num_ttr:" + data.numAttributes());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		addFeatures();
		saveFile(data,"D:/OneDrive/Documents/thesis/data/test_data1.1.arff");
	}
}