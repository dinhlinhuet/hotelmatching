package creatingdata;

import handlingfile.ManipulateFile;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.*;
import java.sql.ResultSet;
import java.util.*;
/*This class create data from arff file*/
public class RandomDataArff extends ManipulateFile{
    static FastVector atts = new FastVector();
    static Instances data;
    static double[] vals;
    static int i;
    static List<String> line_temp ;
    ArrayList<FastVector> list_vector = new ArrayList<>();
    ArrayList<String> nameAttributes = new ArrayList<>();
    static ArrayList<Attribute> attributes = new ArrayList<>();
    static HashSet hashSetLines = new HashSet();
    ResultSet resultSet = null;

    public void createRandomData() {
        ArffLoader loader = new ArffLoader();
        try {
            loader.setFile(new File("D:/mega folder/thesis/data.arff"));
            Instances data = loader.getDataSet();
            for (i = 0; i < 6; i++) {
                attributes.add(new Attribute(data.attribute(i).name(),
                        (FastVector) (null)));
                atts.addElement(attributes.get(i));
            }
            for (i = 6; i < 12; i++) {
                attributes.add(new Attribute(data.attribute(i).name()));
                atts.addElement(attributes.get(i));
            }
            FastVector myNomVals = new FastVector();
            myNomVals.addElement("0");
            myNomVals.addElement("1");
            attributes.add(new Attribute("unique", myNomVals));
            atts.addElement(attributes.get(12));

            Instances data1 = new Instances("MyRelation", atts, 0);
            int k = 0, j = 0, i = 0, l = 0;
            while (k< data.numInstances()) {
                vals = new double[data1.numAttributes()];
                for (j = 0; j < 6; j++) {
                    vals[j] = data1.attribute(j).addStringValue(data1.instance(k).stringValue(j));
                }
                for (j = 6; j < 12; j++) {
                    vals[j] = Double.parseDouble(data1.attribute(k).value(j));
                }
                if (data1.instance(k).stringValue(12).equals("0")) {
                    vals[12] = data.attribute(12).indexOfValue("0");
                } else {
                    vals[12] = data.attribute(12).indexOfValue("1");
                }

                Instance instance = new Instance(1.0, vals);
                if (vals[12] == 1) {
                    data.add(instance);
                }
                k++;
            }
            line_temp = new ArrayList<>(hashSetLines);
            Collections.shuffle(line_temp);
            Random random = new Random();
            ArrayList list_random = new ArrayList();
            String temp = "";
            k = 0;
            while (k < 1521) {
                int rand = random.nextInt(data.numInstances());
                vals = new double[data.numAttributes()];
                for (j = 0; j < 6; j++) {
                    vals[j] = data.attribute(j).addStringValue(data1.instance(k).stringValue(j));
                }
                for (j = 6; j < 12; j++) {
                    vals[j] = data1.instance(k).value(j);
                }
                vals[12] = data1.attribute(12).indexOfValue("0");
                Instance instance = new Instance(1.0, vals);
                data.add(instance);
                k++;
            }
            System.out.println(k);
            System.out.println("duplicate" + l);
            System.out.println("number_instances: " + data.numInstances());
            System.out.println("num_ttr:" + data.numAttributes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        RandomDataArff randomDataArff = new RandomDataArff();
        randomDataArff.createRandomData();
        System.out.println();
        saveFile(data, "D:/mega folder/thesis/data/test_data_sub1.1.arff");
    }
}