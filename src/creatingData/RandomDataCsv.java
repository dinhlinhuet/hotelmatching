package creatingdata;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.*;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import static handlingfile.ManipulateFile.saveFile;
/*This class to create random data from file csv*/
public class RandomDataCsv {
    static FastVector atts = new FastVector();
    ;
    static Instances data;
    static double[] vals;
    int i;
    static List<String> line_temp ;
    ArrayList<FastVector> list_vector = new ArrayList<>();
    ArrayList<String> nameAttributes = new ArrayList<>();
    static ArrayList<Attribute> attributes = new ArrayList<>();
    static HashSet hashSetLines = new HashSet();
    ResultSet resultSet = null;

    public static void createRandomData() {
        String csvFile = "D:/OneDrive/Documents/thesis/data/hotel_Sheet1 (2).csv";
        BufferedReader br = null;
        String line = "";
        List<String> lines = new ArrayList<String>();
        String cvsSplitBy = "@@";
        String[] title = new String[10];
        String[] value;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            int k = 0, j = 0, i = 0, l = 0;
            while ((line = br.readLine()) != null) {
                if (k == 0) {
                    title = line.split(cvsSplitBy);
                    for (i = 0; i < 6; i++) {
                        attributes.add(new Attribute(title[i],
                                (FastVector) (null)));
                        atts.addElement(attributes.get(i));
                    }
                    for (i = 6; i < 12; i++) {
                        attributes.add(new Attribute(title[i]));
                        atts.addElement(attributes.get(i));
                    }
                    FastVector myNomVals = new FastVector();
                    myNomVals.addElement("0");
                    myNomVals.addElement("1");
                    attributes.add(new Attribute("unique", myNomVals));
                    atts.addElement(attributes.get(12));
                    data = new Instances("MyRelation", atts, 0);
                } else {
                    vals = new double[data.numAttributes()];
                    value = line.split(cvsSplitBy);
                    for (j = 0; j < 6; j++) {
                        vals[j] = data.attribute(j).addStringValue(value[j]);
                    }
                    for (j = 6; j < 12; j++) {
                        vals[j] = Double.parseDouble(value[j]);
                    }
                    if (value[12].equals("0")) {
                        vals[12] = data.attribute(12).indexOfValue("0");
                        hashSetLines.add(line);
                        lines.add(line);
                    } else {
                        vals[12] = data.attribute(12).indexOfValue("1");
                    }
                    Instance instance = new Instance(1.0, vals);
                    if (vals[12] == 1) {
                        data.add(instance);
                    }
                }
                k++;
            }
            line_temp = new ArrayList<>(hashSetLines);
            Collections.shuffle(line_temp);
            Random random = new Random();
            ArrayList list_random = new ArrayList();
            String temp = "";
            k = 0;
            while (k < 1521) {
                int rand = random.nextInt(lines.size());
                temp = line_temp.get(k);
                value = temp.split(cvsSplitBy);
                vals = new double[data.numAttributes()];
                for (j = 0; j < 6; j++) {
                    vals[j] = data.attribute(j).addStringValue(value[j]);
                }
                for (j = 6; j < 12; j++) {
                    vals[j] = Double.parseDouble(value[j]);
                }
                vals[12] = data.attribute(12).indexOfValue("0");
                Instance instance = new Instance(1.0, vals);
                data.add(instance);
                k++;
            }
            System.out.println(k);
            System.out.println("duplicate" + l);
            System.out.println("number_instances: " + data.numInstances());
            System.out.println("num_ttr:" + data.numAttributes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) throws Exception {
        createRandomData();
        System.out.println();
        saveFile(data,"D:/OneDrive/Documents/thesis/data/test_data_sub1.1.arff");
    }
}